#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include device/lge/vu2u-common/BoardConfigCommon.mk

#OTA
TARGET_OTA_ASSERT_DEVICE := vu2,vu2u,vu2us,vu2ul,vu2uk,f260s

#Header
TARGET_SPECIFIC_HEADER_PATH := device/lge/vu2u/include

#Bootloader
TARGET_BOOTLOADER_BOARD_NAME := msm8960
TARGET_BOOTLOADER_NAME=F200

#Bluetooth
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := device/lge/vu2u/bluetooth
BOARD_HAVE_BLUETOOTH := true
BOARD_HAVE_BLUETOOTH_BCM := true
BOARD_BLUEDROID_VENDOR_CONF := device/lge/vu2u/bluetooth/vnd_vu2u.txt

# Wifi related defines
BOARD_WLAN_DEVICE := bcmdhd
BOARD_WLAN_DEVICE_REV := bcm4334
WIFI_DRIVER_FW_PATH_PARAM   := "/sys/module/bcmdhd/parameters/firmware_path"
WIFI_DRIVER_FW_PATH_STA          := "/system/etc/firmware/fw_bcmdhd.bin"
WIFI_DRIVER_FW_PATH_AP           := "/system/etc/firmware/fw_bcmdhd_apsta.bin"
WIFI_DRIVER_FW_PATH_P2P := "/system/etc/firmware/fw_bcmdhd_p2p.bin"
WIFI_BAND := 802_11_ABG
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_$(BOARD_WLAN_DEVICE)
BOARD_HOSTAPD_PRIVATE_LIB := lib_driver_cmd_$(BOARD_WLAN_DEVICE)
WPA_SUPPLICANT_VERSION := VER_0_8_X
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
BOARD_HOSTAPD_DRIVER := NL80211

#Recovery
TARGET_RECOVERY_PIXEL_FORMAT := RGBX_8888
BOARD_USE_CUSTOM_RECOVERY_FONT:= \"roboto_15x24.h\"
TARGET_RECOVERY_FSTAB = device/lge/vu2u/fstab.fx1sk
ENABLE_LOKI_RECOVERY := true
BOARD_RECOVERY_SWIPE := true

# Camera
COMMON_GLOBAL_CFLAGS += -DLG_CAMERA_HARDWARE
USE_CAMERA_STUB := true
USE_DEVICE_SPECIFIC_CAMERA:= true

#LibLight
TARGET_PROVIDES_LIBLIGHT= true

-include vendor/lge/vu2u/BoardConfigVendor.mk
